package com.example.openapi_processor.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.example.openapi_processor.service.OpenApiProcessor;

public class OpenApiGui extends JFrame {

    private OpenApiProcessor openApiProcessor;
    private JTextField inputFilePathField;
    private JTextField outputFilePathField;
    private JButton processButton;

    public OpenApiGui() {
        openApiProcessor = new OpenApiProcessor();

        setTitle("OpenAPI Processor");
        setSize(400, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        inputFilePathField = new JTextField(20);
        outputFilePathField = new JTextField(20);
        processButton = new JButton("Process");

        processButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                processOpenApiFile();
            }
        });

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        panel.add(new JLabel("Input File Path:"));
        panel.add(inputFilePathField);
        panel.add(new JLabel("Output File Path:"));
        panel.add(outputFilePathField);
        panel.add(processButton);

        add(panel);
    }

    private void processOpenApiFile() {
        String inputFilePath = inputFilePathField.getText().trim();
        String outputFilePath = outputFilePathField.getText().trim();

        if (inputFilePath.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Input file path is required.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (outputFilePath.isEmpty()) {
            outputFilePath = generateDefaultOutputFileName(inputFilePath);
        }

        try {
            openApiProcessor.processOpenApiFile(inputFilePath, outputFilePath);
            JOptionPane.showMessageDialog(this, "OpenAPI file processed successfully. Output file: " + outputFilePath, "Success", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error processing OpenAPI file: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            e.printStackTrace();
        }
    }

    private String generateDefaultOutputFileName(String inputFilePath) {
        String inputFileName = Paths.get(inputFilePath).getFileName().toString();
        String baseName = inputFileName.replaceFirst("[.][^.]+$", "");
        String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        return baseName + "_processed_" + timestamp + ".yaml";
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            OpenApiGui openApiGui = new OpenApiGui();
            openApiGui.setVisible(true);
        });
    }
}