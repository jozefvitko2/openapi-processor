package com.example.openapi_processor.service;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Map;

public class OpenApiProcessor {

    public void processOpenApiFile(String inputFilePath, String outputFilePath) {
        try (InputStream inputStream = new FileInputStream(inputFilePath)) {
            // Load the source YAML data
            Map<String, Object> sourceData = loadYaml(inputStream);
            // Create a deep copy of the source data for the output
            Map<String, Object> outputData = deepCopy(sourceData);

            // Inline all references
            inlineAllReferences(outputData, sourceData);

            // Ensure the output directory exists
            File outputFile = new File(outputFilePath);
            outputFile.getParentFile().mkdirs();

            // Write the output YAML data
            try (Writer writer = new FileWriter(outputFile)) {
                writeYaml(outputData, writer);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Map<String, Object> loadYaml(InputStream inputStream) {
        Yaml yaml = new Yaml(new Constructor(Map.class));
        return yaml.load(inputStream);
    }

    private void writeYaml(Map<String, Object> yamlData, Writer writer) {
        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Yaml outputYaml = new Yaml(options);
        outputYaml.dump(yamlData, writer);
    }

    private Map<String, Object> deepCopy(Map<String, Object> original) {
        Yaml yaml = new Yaml();
        String serialized = yaml.dump(original);
        return yaml.load(serialized);
    }

    private void inlineAllReferences(Map<String, Object> outputData, Map<String, Object> sourceData) {
        if (outputData.containsKey("components")) {
            Map<String, Object> components = (Map<String, Object>) outputData.get("components");
            if (components.containsKey("responses")) {
                Map<String, Object> responses = (Map<String, Object>) components.get("responses");
                if (responses != null) {
                    processPathsForInlining(outputData, sourceData);
                }
            }
        }
    }

    private void processPathsForInlining(Map<String, Object> outputData, Map<String, Object> sourceData) {
        if (outputData.containsKey("paths")) {
            Map<String, Object> paths = (Map<String, Object>) outputData.get("paths");
            for (Map.Entry<String, Object> pathEntry : paths.entrySet()) {
                processMethodsForInlining((Map<String, Object>) pathEntry.getValue(), sourceData);
            }
        }
    }

    private void processMethodsForInlining(Map<String, Object> methods, Map<String, Object> sourceData) {
        for (Map.Entry<String, Object> methodEntry : methods.entrySet()) {
            Map<String, Object> method = (Map<String, Object>) methodEntry.getValue();
            if (method.containsKey("responses")) {
                inlineMethodResponses((Map<String, Object>) method.get("responses"), sourceData);
            }
        }
    }

    private void inlineMethodResponses(Map<String, Object> methodResponses, Map<String, Object> sourceData) {
        for (Map.Entry<String, Object> responseEntry : methodResponses.entrySet()) {
            Object response = responseEntry.getValue();
            if (response instanceof Map && ((Map<?, ?>) response).containsKey("$ref")) {
                inlineResponse(responseEntry, (Map<String, Object>) response, methodResponses, sourceData);
            } else if (response instanceof Map) {
                inlineNestedReferences((Map<String, Object>) response, sourceData);
            }
        }
    }

    private void inlineResponse(Map.Entry<String, Object> responseEntry, Map<String, Object> response,
                                Map<String, Object> methodResponses, Map<String, Object> sourceData) {
        String ref = (String) response.get("$ref");
        if (ref.startsWith("#/components/responses/")) {
            String refKey = ref.substring("#/components/responses/".length());
            Map<String, Object> responses = (Map<String, Object>) ((Map<String, Object>) sourceData.get("components")).get("responses");
            if (responses.containsKey(refKey)) {
                methodResponses.put(responseEntry.getKey(), deepCopy((Map<String, Object>) responses.get(refKey)));
                inlineNestedReferences((Map<String, Object>) methodResponses.get(responseEntry.getKey()), sourceData);
            }
        }
    }

    private void inlineNestedReferences(Map<String, Object> response, Map<String, Object> sourceData) {
        if (response.containsKey("content")) {
            Map<String, Object> content = (Map<String, Object>) response.get("content");
            for (Map.Entry<String, Object> contentEntry : content.entrySet()) {
                Map<String, Object> mediaType = (Map<String, Object>) contentEntry.getValue();
                if (mediaType.containsKey("schema")) {
                    Map<String, Object> schema = (Map<String, Object>) mediaType.get("schema");
                    if (schema.containsKey("$ref")) {
                        String ref = (String) schema.get("$ref");
                        if (ref.startsWith("#/components/schemas/")) {
                            String refKey = ref.substring("#/components/schemas/".length());
                            Map<String, Object> schemas = (Map<String, Object>) ((Map<String, Object>) sourceData.get("components")).get("schemas");
                            if (schemas.containsKey(refKey)) {
                                mediaType.put("schema", deepCopy((Map<String, Object>) schemas.get(refKey)));
                            }
                        }
                    }
                }
            }
        }
    }
}